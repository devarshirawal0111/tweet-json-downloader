from app import app
from flask import render_template, request, send_file, send_from_directory, Response
from app.twitterPy import SearchForm
from app.twitterPy import getTweets
import json


@app.route('/index')
@app.route('/')
def index():
    return render_template('base2.html')
    
    
@app.route('/search', methods=['GET','POST'])
def search():
    form = SearchForm()
    if form.is_submitted():
        #print(request.form['query'])
        if request.form['ntweets']:
            n = int(request.form['ntweets'])
         
        else:
            n = 100
        
        if form.time1.data==None or form.time2.data==None:
            time1_str = ""
            time2_str = ""
        else:
            time1 = form.time1.data
            time1_month = time1.month
            time1_day = time1.day
            time1_year = time1.year
            time1_str = "{year}-{month}-{day}".format(year=time1_year, month=time1_month, day=time1_day)
            
            time2 = form.time2.data
            time2_month = time2.month
            time2_day = time2.day
            time2_year = time2.year
            time2_str = "{year}-{month}-{day}".format(year=time2_year, month=time2_month, day=time2_day)
        
        data = json.loads(getTweets(q=request.form['query'], lang=request.form['language'], geocode=request.form['location'], n=n, time1= time1_str, time2= time2_str))
        all_data = {}
        all_data['data']=data
        all_data['query']=request.form['query']
        all_data['language']=request.form['language']
        
        
        with open('./app/download.json','w', encoding='utf-8') as file:
        	json.dump(all_data, file, indent=4, ensure_ascii=False)
        #print(json.dumps(data))
        print(request.form['query'], request.form['language'], request.form['location'], time1_str, time2_str)
        return render_template('twitterSearch.html', data=data, form=form, _tweet_ = True)
    return render_template('twitterSearch.html', form = form)


@app.route('/download/')
def download():
    with open('./app/download.json', 'r') as file:
        content = json.loads(file.read())

    query = content['query']
    language = content['language']
    data = content['data'] 
   
	
    try:
        return Response(json.dumps(data), mimetype="application/json",headers={"Content-Disposition": "attachment; filename={}.json".format(query + "_" + language)})
    except Exception as e:
        return str(e)
