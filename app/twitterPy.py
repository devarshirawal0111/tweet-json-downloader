import tweepy
import json
from flask_wtf import FlaskForm
from wtforms import StringField, SelectField, SubmitField
from wtforms.fields.html5 import DateField, SearchField
from wtforms.validators import DataRequired
import app.config as config


auth = tweepy.OAuthHandler(config.consumer_key, config.consumer_secret)
auth.set_access_token(config.access_token, config.access_token_secret)

api = tweepy.API(auth)

# getTrends = api.trends_available()

# with open('./Data/trends.json', "w", encoding = 'utf-8') as file:
#     json.dump(getTrends, file, indent=4, ensure_ascii=False)

# locTrends = []
# for i in range(0,6):
#     #print(getTrends[i]['name'])
#     temp = {'location' : getTrends[i]['name'], 'trend' : api.trends_place(id = getTrends[i]['woeid'])}
#     locTrends.append(temp)

# for i in range(0,6):
#     print(locTrends[i]['location'])

# with open('./Data/locTrends.json', "w", encoding = 'utf-8') as file:
#     json.dump(locTrends, file, indent=4, ensure_ascii=False)

# indiaTrends = api.trends_place(id = '2282863')
# with open('./Data/indiaTrends.json', "w", encoding = 'utf-8') as file:
#     json.dump(indiaTrends, file, indent=4, ensure_ascii=False)

def getTweets(q, lang = 'en', geocode = None, n=10, time1=None, time2=None):
    if time1==None or time2==None:
        time1 = ""
        time2 = ""
    #print(q, lang, geocode)
    result = []
    all = []
    # result = api.search(q=q, geocode=geocode, lang=lang, tweet_mode='extended')
    for tweet in tweepy.Cursor(api.search, q=q, lang=lang, geocode=geocode, result_type="recent", tweet_mode='extended', since=time1, until=time2).items(n):
        tweet = tweet._json
        if 'retweeted_status' in tweet:
    #         #print(json.dumps(tweet))
            #tweet['full_text'] = tweet['full_text'].split()
            #print(tweet['retweeted_status']['full_text'])
            #temp = tweet['retweeted_status']['full_text'].split()
            #temp = " ".join(tep[2:])
            tweet['full_text'] = " ".join((tweet['retweeted_status']['full_text'].split())[:])
            #print(tweet['full_text'])
        else:
            #print(tweet['full_text'])
            tweet['full_text'] = " ".join((tweet['full_text'].split()))
            #print(tweet['full_text'])
        tweet['URL'] = "https://twitter.com/" + tweet['user']['screen_name'] + "/status/" + tweet['id_str']

        result.append(tweet)
    # for tweet in result['statuses']:
    #     tweet['full_text'] = api.get_status(id=(tweet['id']), tweet_mode='extended')['full_text']
    
    return json.dumps(result)

def search(content):
    result = api.search(q=content,count=10, tweet_mode='extended')
    for tweet in result['statuses']:
        if 'retweeted_status' in tweet:
            #print(json.dumps(tweet))
            #tweet['full_text'] = tweet['full_text'].split()
            print(tweet['retweeted_status']['full_text'])
            #temp = tweet['retweeted_status']['full_text'].split()
            #temp = " ".join(tep[2:])
            tweet['full_text'] = " ".join((tweet['retweeted_status']['full_text'].split())[:])
            #print(tweet['full_text'])
        else:
            print(tweet['full_text'])
            tweet['full_text'] = " ".join((tweet['full_text'].split()))
            #print(tweet['full_text'])
    # for tweet in result['statuses']:
    #     tweet['full_text'] = api.get_status(id=(tweet['id']), tweet_mode='extended')['full_text']
    return json.dumps(result['statuses'])

languague_list = [('en', 'English'), ('de', 'German'), ('hi', 'Hindi'), ('gu', 'Gujarati')]
locations = [('42.76710,-96.15628,1000km', 'USA'), \
            ('21.48642,69.93125,150km', 'Gujarat'), \
            (' 47.65985,9.16862,300km', 'Germany'), \
            ('23.31609,72.4312,700mi', 'India')]
class SearchForm(FlaskForm):
    query = SearchField('Search', validators=[DataRequired()])
    location = SelectField('Location', choices=locations)
    time1 = DateField('From Date', format="%Y-%m-%d")
    time2 = DateField('Till Date', format='%Y-%m-%d')
    language = SelectField('Language', choices=languague_list)
    submit = SubmitField('Search')
    ntweets = StringField('No. of Tweets')